# Agglutination Image DB

## Requirements and setup

- Python 3.8

We'll describe our setup using Python virtual environments:

1. Create and activate your Python virtual environment.

      a. For the commands to do this, see: https://docs.python.org/3/library/venv.html

2. Install all package dependencies using `pip install -r requirements.txt`.


## Running a single example

Run the following in the ./agglutination-image-db directory:

`bash download_process_upload.sh`

## NOTE: Updating the spreadsheet

When you are updating the spreadsheet with code for the first time, you must log into a google account. Any google account that has access to the spreadsheet will do.
For an example account, you can use: 
<br/>
username: testagglutination@gmail.com
<br/>
password:Microfluidics871#

<br/>

## Project Structure
The project starts with `download_process_upload.sh`, which does three things:
1. Download any new tray folders from ownCloud (if they're not available locally)
2. Iterate through all non-processed trays, process them (i.e, save them into individual well images), and then record that the tray has been processed.
3. Upload each new individual well image to ownCloud, and update a public spreadsheet so they can be read from our website.




## Warning:

The process of downloading and uploading files is slow. This is because we must download / upload each image one-by-one. I unfortunately could not find an easy fix for this. If it's a huge problem let me know (sid.gupta@mail.utoronto.ca) and I can look into optimizations.