
# Make sure we have these two directories (they'll be ignored by git, and only kept locally)
mkdir -p full_tray_imgs;
mkdir -p ../individual_well_imgs;

# First, download new tray_images from owncloud, that aren't already stored locally
# echo "Downloading images from opencloud"
cd full_tray_imgs;
# python ../processing_files/download_owncloud_imgs.py;


# Next, begin processing all trays (that haven't already been processed yet) into individual well images.
cd ../processing_files;
for tray_imgs_dirpath in ../full_tray_imgs/*;
do
    # If we have not processed this tray before:
    if ! grep -q $tray_imgs_dirpath ../processed_folders.txt; then
        # Check that this tray folder has a meta.yml file. If not, record that there's no metadata in this folder and skip it.
        if [ ! -f $tray_imgs_dirpath/meta.yml ]; then
            echo "Could not find a meta.yml file in {$tray_imgs_dirpath}"
            echo $tray_imgs_dirpath > no_meta_yml.txt;
        fi
        echo "Processing wells in {$tray_imgs_dirpath}";
        # Process the tray
        python ./process_trays_into_indiv_wells.py --config_file $tray_imgs_dirpath/meta.yml --tray_imgs_dirpath=$tray_imgs_dirpath  --indiv_wells_dir ../individual_well_imgs;
        # Record that it's been processed
        echo $tray_imgs_dirpath >> ../processed_folders.txt;
    fi
    echo "Finished processing wells in {$tray_imgs_dirpath}";
done;


# Finally, upload these new individual wells to ownCloud
echo "Uploading individual well images to ownCloud.";
cd ../individual_well_imgs;
python ../processing_files/upload_indiv_imgs.py;
