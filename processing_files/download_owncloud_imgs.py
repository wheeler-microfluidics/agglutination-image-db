import owncloud
import os
import tqdm
from tqdm import tqdm
from pathlib import Path


def create_path(path, directory=False):
    if directory:
        dir = path
    else:
        dir = os.path.dirname(path)
    if not os.path.exists(dir):
        print(f'{dir} does not exist, creating')
        try:
            os.makedirs(dir)
            return True
        except Exception as e:
            print(e)
            print(f'Could not create path {path}')
            return False
    return False


def download_from_oc():
    last_path_dir = str(Path.cwd()).split('/')[-1]
    print(last_path_dir)
    if last_path_dir != 'full_tray_imgs':
        raise Exception("Must be in the full_tray_imgs directory to use this script.")

    public_link = 'https://cloud.digiomics.com/index.php/s/vddc0Nq9CP3ereI'

    oc = owncloud.Client.from_public_link(public_link)

    oc_dirlist = oc.list('.')
    curr_dirlist_names = os.listdir('.')
    oc_dirlist_names = [oc_dir.path.split('/')[1] for oc_dir in oc_dirlist]
    oc_dirlist_names = [oc_dirname for oc_dirname in oc_dirlist_names if oc_dirname not in curr_dirlist_names]

    for oc_dirname in tqdm(oc_dirlist_names):
        new_dir = create_path(oc_dirname, directory=True)
        if new_dir:
            print(f"Downloading {oc_dirname}")
            img_list = oc.list(f"/{oc_dirname}/")
            for img in tqdm(img_list):
                imgname = img.path.split('/')[2]
                oc.get_file(img.path, f"{oc_dirname}/{imgname}")


download_from_oc()