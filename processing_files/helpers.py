
def get_position_map():
    """
        Returns a dictionary that maps tray-coordinates (e.g, B2, A3) to (y, x) coordinates.
        Our current code does a rotate and flip to the tray, so this dictionary maps
        coordinates after that rotate and flip.
    """

    # In this definition, A1 is in the top-left corner
    position_map = {}
    for y_ind, y_label in enumerate('ABCDEFGH'):
        for x_ind, x_label in enumerate(range(1, 13)):
            position_map[f"{y_label}{x_label}"] = (y_ind, x_ind)
    return position_map
    
POSITION_MAP = get_position_map()
INV_POSITION_MAP = {v: k for k, v in POSITION_MAP.items()}

def compute_fixed_attrs_str(fixed_attr_dict):
    fixed_attr_str = ''
    if fixed_attr_dict != {}:
        fixed_attr_str += '_fixed'
        for attr_name in fixed_attr_dict:
            fixed_attr_str += f"_{attr_name}"
            fixed_attr_str += f"_{fixed_attr_dict[attr_name]}"
    return fixed_attr_str