import numpy as np
import pandas as pd
from helpers import POSITION_MAP

class Well:
    """
        Defines an Well instance, which represents a single well
        in the agglutination tray. 
        Attributes:
            - self.well_image: The image that represents this well. 
            - self.feature_vec: The feature vector that represents this well. This is used to compute it's agglutination score.
            - self.well_image_original: A copy of the original well_image, before it goes through it's processing. This is
                optionally stored if it's needed to compute a feature; otherwise it's set to None.
            - self.position: The position of the well in the WellMatrix
            - self.contours: Contours for this well (used in feature extraction)
            - self.concentration,
            - self.buffer,
            - self.beads,
            - self.saliva,
            - self.additives,
            - self.other:
                All attributes from the well_metadata file
    """

    def __init__(self, well_image, feature_vec_size, should_save_original, position, group_num, group_data):
        """
            Instantiates a Well object.
        """
        self.well_image = well_image
        self.feature_vec = np.zeros(feature_vec_size)
        self.position = position
        self.contours = None
        self.group_num = group_num
        self.concentration = group_data['concentration']
        self.buffer = group_data['buffer']
        self.beads = group_data['beads']
        self.saliva = group_data['saliva']
        self.additives = group_data['additives']
        self.other = group_data['other']
        self.well_image_original = None
        if should_save_original:
            self.well_image_original = self.well_image.copy()

    def get_image_alias(self):
        """
            Return an alias of this Well's image representation. Note that mutating this alias
            will mutate this attribute.
        """
        return self.well_image

    def update_feature_vec(self, feature_val, feature_ind):
        """
            Update this Well's feature vector.
        """
        self.feature_vec[feature_ind] = feature_val

    def set_contours(self, contours):
        """
            Update the contours in this well.
        """
        self.contours = contours


class WellMatrix():
    """
        Defines an WellMatrix instance, which represents an agglutination tray
        and it's selected wells for processing.

        Attributes:
            - self.tray_width: Width of the tray
            - self.tray_height: Height of the tray
            - self.num_frames: The total number of frames that this tray is observed over.
            - self.well_img_radius: The radius of each well image. Each well image will be a square with sidelength self.well_img_radius.
            - self.all_selected_well_coords: A (N, 2) matrix that contains the (y, x) coordinates of all selected
                wells for processing.
            - self.shape: The shape of the WellMatrix representation
            - self.well_matrix_data: The actual data-structure implemented for this WellMatrix. We use a 'matrix-like' interface,
                however, the actual implementation is a dataframe. This allows us to support intuitive iteration 
                (e.g, iterate over all rows, cols, and frames); as well as perform complex queries
                (e.g, get me all wells with concentration Y).
    """
    

    def __init__(self, well_np_matrix: np.ndarray, well_metadata: dict, feature_vec_size: int, should_save_original: bool) -> None:
        """
            Arguments:
                - well_np_matrix: an np array representing the well-tray. it's dimensions represent
                  (tray_height, tray_width, num_frames, well_img_radius, well_img_radius)
        """
        self.tray_height, self.tray_width, self.num_frames, self.well_img_radius, _ = well_np_matrix.shape
        self.shape = well_np_matrix.shape
        self.all_selected_well_coords = [POSITION_MAP[well_coord] for well_coord in well_metadata.sample_wells]
        # Transform the numpy well representation into a dataframe implementation
        df_template = {'group': [], 'frame': [], 'concentration': [], 'buffer': [], 'beads': [], 'saliva': [], 'additives': [], 'other': [], 'well_object': []}
        df_indices = []

        # For this code only, we set the attributes to filler values.



        # Iterate through all groups 
        for group in well_metadata.groups:
            group_data = well_metadata.groups[group]
            group_data['concentration'] = "0"
            group_data['buffer'] = "0"
            group_data['beads'] = "0"
            group_data['saliva'] = "0"
            group_data['additives'] = "0"
            group_data['other'] = "0"
            group_coords = [POSITION_MAP[well_coord] for well_coord in group_data['replicates']]
            # For each well in this group, create a Well object, and add it to the dataframe
            for well_y, well_x in group_coords:
              for frame in range(self.num_frames):
                self._append_to_df_data(
                    df_template, 
                    df_indices, 
                    well_y, 
                    well_x, 
                    frame, 
                    group, 
                    group_data, 
                    Well(
                        well_np_matrix[well_y, well_x, frame], 
                        feature_vec_size, should_save_original, 
                        (well_y, well_x, frame), 
                        group,
                        group_data
                    )
                )
        # The WellMatrix will have a dataframe implementation, so that we can perform queries (such as 'get me all wells with concentration Y').
        self.well_matrix_data = pd.DataFrame(df_template, df_indices)


    def _append_to_df_data(self, df_template, df_indices, well_y, well_x, frame, group, group_data, well_object):
        """
            Helper function used in the initializer to populate the dataframe with a Well.
        """
        df_indices.append(str((well_y, well_x, frame)))
        df_template['group'].append(group)
        df_template['frame'].append(frame)
        df_template['concentration'].append(group_data['concentration'])
        df_template['buffer'].append(str(group_data['buffer']))
        df_template['beads'].append(str(group_data['beads']))
        df_template['saliva'].append(str(group_data['saliva']))
        df_template['additives'].append(str(group_data['additives']))
        df_template['other'].append(str(group_data['other']))
        df_template['well_object'].append(
            well_object
        )


    def __getitem__(self, indices: int or tuple) -> Well:
        """
            Allows us to do matrix indexing with a WellMatrix object.
        """
        if type(indices) == int:
            indices = (indices)
        dim = len(indices)
        if dim > 3:
            raise IndexError("Maximum dimension size of a WellMatrix is 3.")
        indices = str(indices)
        if indices not in self.well_matrix_data.index.values:
            # raise IndexError('Index specified is not a selected well. Although this is a 3D matrix, for space efficiency we only store the selected wells.')
            return None
        return self.well_matrix_data.loc[indices, 'well_object']


    def get_attr_vals(self, attr_name):
        """
            Get all the unique values of a certain attribute (concentration, groups, etc.) from the WellMatrix
        """
        return self.well_matrix_data[attr_name].unique()


    def get_wells_with_attr(self, attr_name, attr_val, frame=0):
        """
            Get all Wells from a frame with a certain attribute
        """
        # Get all rows from the specified frame
        selected_rows = self.well_matrix_data.loc[self.well_matrix_data['frame'] == frame]
        return selected_rows.loc[self.well_matrix_data[attr_name] == attr_val]['well_object'].values


    def get_wells_with_attr_combs(self, attr_dict, frame=0):
        """
            Get all Wells from a frame with a combination of attributes
        """
        # Get all rows from the specified frame
        selected_rows = self.well_matrix_data.loc[self.well_matrix_data['frame'] == frame]
        for attr_name in attr_dict:
            selected_rows = selected_rows.loc[self.well_matrix_data[attr_name] == attr_dict[attr_name]]
        return selected_rows['well_object'].values

