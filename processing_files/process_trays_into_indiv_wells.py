import numpy as np
import argparse
from os import path
import matplotlib.pyplot as plt

# Import the functions from the files
from experiment_helpers import get_config
from compute_np_well_matrix import compute_np_well_matrix
from well_matrix import WellMatrix
from helpers import INV_POSITION_MAP

from tqdm import trange

class ExperimentRunner():
    """
        Defines an ExperimentRunner instance, which manages how a well matrix
        is read and preprocessed.

        Attributes:
            - self.well_metadata: The metadata associated with the provided tray. It's read from the meta.yml file.
            - self.well_matrix: A matrix of size (tray_height, tray_width, num_frames, well_height, well_width).
                This stores all the wells inside an image dataset.
    """


    def __init__(self, well_metadata: dict) -> None:
      """
        Sets the options that are read by the meta.yml config file.
      """
      self.well_metadata = well_metadata


    def _create_well_matrix(self, tray_imgs_dirpath: str) -> None:
        """
            Transforms the LAT tray into a numpy matrix that stores each well.
        """
        tray_imgs_dirname = tray_imgs_dirpath.split('/')[-1]
        saved_well_matrices_path = f"./saved_well_matrices/{tray_imgs_dirname}"
        well_matrix_path = saved_well_matrices_path
        if path.exists(f"{well_matrix_path}.npz"):
            well_np_matrix = np.load(f"{well_matrix_path}.npz")['data']
            print("Finished loading well matrix")
        else:
            print("Computing well_matrix.")
            well_np_matrix = compute_np_well_matrix(tray_imgs_dirpath, 8, 12, 91, self.well_metadata.A1_top)
            print('Finished creating well matrix')
            # np.savez(f"{well_matrix_path}", data=well_np_matrix)
        self.well_matrix = WellMatrix(well_np_matrix, self.well_metadata, 0, False)


    def run(self, tray_imgs_dirpath, indiv_wells_dir) -> None:
        """
            Saves each individual well as a .png file.
        """
        tray_imgs_dirname = tray_imgs_dirpath.split('/')[-1]
        self._create_well_matrix(tray_imgs_dirpath)

        # Iterate through all selected wells, and save the individual images.
        plt.figure(figsize=(8, 8))
        for frame in trange(self.well_matrix.shape[2]):
            for well_y, well_x in self.well_matrix.all_selected_well_coords:
                curr_well = self.well_matrix[well_y, well_x, frame]
                # Save the curr_well as it's own image
                plt.tight_layout()
                plt.axis('off')
                plt.imshow(curr_well.get_image_alias(), cmap='gray')
                plt.tight_layout()
                plt.savefig(f"{indiv_wells_dir}/{tray_imgs_dirname}_{INV_POSITION_MAP[(well_y, well_x)]}_{frame}")
                plt.clf()


def main(args):
    """
        Given the command line arguments, run the experiments and plot results.
    """
    well_metadata = get_config(args.indiv_wells_dir, args.config_file)
    runner = ExperimentRunner(well_metadata)
    runner.run(args.tray_imgs_dirpath, args.indiv_wells_dir)


if __name__ == '__main__':
    """
        Specifies the command line arguments to configure the different experiments.
    """
    parser = argparse.ArgumentParser(description="For running different kinds of feature-extraction experiments on LAT trays.")
    parser.add_argument('--config_file', type=str, required=True)
    parser.add_argument('--indiv_wells_dir', type=str, required=True)
    parser.add_argument('--tray_imgs_dirpath', type=str, required=True)
    args = parser.parse_args()
    main(args)
