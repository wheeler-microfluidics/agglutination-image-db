import os
import sys
import yaml
from types import SimpleNamespace

def create_path(path, directory=False):
    if directory:
        dir = path
    else:
        dir = os.path.dirname(path)
    if not os.path.exists(dir):
        print(f'{dir} does not exist, creating')
        try:
            os.makedirs(dir)
        except Exception as e:
            print(e)
            print(f'Could not create path {path}')


def get_config(indiv_wells_dir, config_file):
    base_path = indiv_wells_dir
    create_path(base_path, directory=True)

    with open(config_file) as f:
        if not os.path.isfile(config_file):
            print(f"File {config_file} does not exist")
            sys.exit(0)
        data = yaml.load(f, Loader=yaml.FullLoader)

    result = SimpleNamespace(**data)
    result.indiv_wells_dir = base_path
    return result