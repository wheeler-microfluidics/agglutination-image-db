import owncloud
import os
import tqdm
from tqdm import tqdm
from pathlib import Path
from update_spreadsheet import append_urls_to_spreadsheet


def upload_to_oc():
    last_path_dir = str(Path.cwd()).split('/')[-1]
    if last_path_dir != 'individual_well_imgs':
        raise Exception("Must be in the individual_well_imgs directory to use this script.")

    public_link = 'https://cloud.digiomics.com/index.php/s/1yMusMFezljkjXK'

    oc = owncloud.Client.from_public_link(public_link)

    oc_imglist = oc.list('.')
    oc_imglist_names = [oc_img.path.split('/')[1] for oc_img in oc_imglist]
    # curr_imglist_names = [curr_imgname for curr_imgname in os.listdir('.') if curr_imgname not in oc_imglist_names]
    curr_imglist_names_temp = [curr_imgname for curr_imgname in os.listdir('.') if curr_imgname in oc_imglist_names and "rabbit" not in curr_imgname]

    # Now begin uploading, and get a list of img urls.
    # First, we upload the images
    print("Uploading images to ownCloud")
    # for curr_imgname in tqdm(curr_imglist_names):
    #     oc.drop_file(curr_imgname)

    # Next, we get the urls to the uploaded images, and update the spreadsheet
    oc.login('asklavounos', 'microfluidics')
    img_url_list = []
    url_prefix = "https://cloud.digiomics.com/index.php/apps/files_sharing/ajax/publicpreview.php?x=3840&y=1184&a=true&file="
    print("Getting image URLS for the spreadsheet from ownCloud")
    for i, curr_imgname in tqdm(enumerate(curr_imglist_names_temp)):
        curr_imgname = curr_imglist_names_temp[i]
        link_info = oc.share_file_with_link(f'/indiv_well_images/{curr_imgname}/')
        token = link_info.token
        img_url = f"{url_prefix}{curr_imgname}&t={token}"
        img_url_list.append(img_url)
        if i == 1000:
            break


    return img_url_list


img_url_list = upload_to_oc()
append_urls_to_spreadsheet(img_url_list)
