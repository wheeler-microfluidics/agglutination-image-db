from __future__ import print_function
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
import numpy as np

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.

# For 20210520_115314 dataset.
SPREADSHEET_ID = '1UBH-A4dQcyGGN1kf3BgPVZDThV7US15MJ3f2udJf5ek'

# For the agreement run
# RANGE_NAME = 'AgreementSheet'
# For storing the reference images
# RANGE_NAME = 'ReferenceImages'
RANGE_NAME = 'ImagesToAnnotate'


def append_urls_to_spreadsheet(img_url_list):
    """
        Appends a list of URLs to our google spreadsheet, if they're not already in there.
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('../processing_files/token.json'):
        creds = Credentials.from_authorized_user_file('../processing_files/token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                '../processing_files/credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('../processing_files/token.json', 'w') as token:
            token.write(creds.to_json())

    # Get the current values in the spreadsheet to check for duplicates.

    service = build('sheets', 'v4', credentials=creds)
    existing_urls = read_spreadsheet(service)

    ## Call the sheets API to upload the URL list.
    # How the input data should be interpreted.
    value_input_option = 'USER_ENTERED'

    # How the input data should be inserted.
    insert_data_option = 'INSERT_ROWS'
    values_to_append = []
    for url in img_url_list:
        if url not in existing_urls:
            values_to_append.append([url])
    value_range_body = {
        "majorDimension": "ROWS",
        "values": values_to_append
    }

    request = service.spreadsheets().values().append(spreadsheetId=SPREADSHEET_ID, range=RANGE_NAME, valueInputOption=value_input_option, insertDataOption=insert_data_option, body=value_range_body)
    response = request.execute()
    print(response)


def read_spreadsheet(service):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID,
                                range=RANGE_NAME).execute()
    values = result.get('values', [])
    values = np.array(values).T.tolist()

    if not values:
        raise Exception('Could not read from the spreadsheet.')
    else:
        return values[0]
    
    