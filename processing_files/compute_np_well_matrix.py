import cv2
import numpy as np
from glob import glob
import matplotlib.pyplot as plt
from tqdm import tqdm

def swap(image):
    """
      Swaps a 3-channel colormap to RGB.
    """
    return cv2.cvtColor(image, cv2.COLOR_BGR2RGB)


def plot_imgs(*args):
    """
      Plots a set or a single image. Can handle RGB or grayscale images.
    """
    nfig = len(args)
    if nfig > 1:
        fig, ax = plt.subplots(1, nfig, figsize=(3*nfig, 5), sharex=True, sharey=True)
    else:
        fig,ax = plt.subplots()
        ax = [ax]
    
    for i,arg in enumerate(args):
        if len(arg.shape) == 3: 
            # Plot RGB Image
            ax[i].imshow(swap(arg))
        else: 
            # Plot grayscale
            ax[i].imshow(arg, 'gray', vmin= 0, vmax=255)
        ax[i].set_axis_off()
    return fig,ax
        

def crop_to_plate(image):
    """
      Crops only the desired plate, and no surrounding black regions.
    """
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blur = cv2.blur(gray, (5,5))
    thresh = cv2.threshold(blur, int(gray.max()*0.7), 255, cv2.THRESH_BINARY)[1]
    cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
    cnts = sorted(cnts, key=cv2.contourArea)
    x,y,w,h = cv2.boundingRect(cnts[-1])
    
    return image[y:y+h, x:x+w].copy()


def rotate_n_flip(image, rot=cv2.ROTATE_90_CLOCKWISE):
    """
      Rotates 90 degrees and flips.
    """
    rotated = cv2.rotate(image, rot)
    return cv2.flip(rotated, 0)


def cor_lum(image):
    """
      Applies localized histogram equalization to each of the image color channels.
    """
    b,g,r = cv2.split(image)
    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(50,50))
    return cv2.merge([clahe.apply(c) for c in (b,g,r)])


def cut_arr(arr, step):
    """
      Used when re-sorting the array of circle centers.
      Parameters:
        arr: Array of (x, y) circle centers
        step: radius, in this case.
    """
    x, y = arr[0]
    # Index all y-positions before this radius
    idx = arr[: ,1]<(y+step)
    # First term: Second term: Truncated array
    return arr[idx], arr[~idx]

def contour_center(contour):
    """
      Gets the center of mass of the contour (I believe).
      This function is not used.
    """
    M = cv2.moments(contour)
    if  M["m00"] > 0:
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        return cX, cY
    else:
        return 0, 0


def read_wells(image, circles, well_matrix, frame, tray_width, radius, erode = 30, cimg=None, ret_img=False,
               font = cv2.FONT_HERSHEY_SIMPLEX):
    """
      Given the assay image and a set of circles representing each well,
      visualize each circle, and compute an agglutination score.
    """
    
    if ret_img:
        if cimg is None:
            cimg = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
        else:
            cimg = cimg.copy()


    h,w = image.shape[:2]
    dims = np.array((w,h))
    
    # Note that there may be multiple hough circles for each well.
    # This makes sure that there's only one, as all the multiple
    # circles get wrapped by one enclosing circle.
    circles = [cv2.minEnclosingCircle(cnt)[0] for cnt in circles ]
    circles = np.asarray(circles).astype('int')

    # Order the circles with respect to their y coordinates.
    circles = circles[circles[:,1].argsort(kind='mergesort')]
    
    trunc_circles = circles.copy()
    new_circles = []
    cut_arr(trunc_circles, radius)
    while len(trunc_circles)>1:
        cut, trunc_circles = cut_arr(trunc_circles, radius)
        cut = cut[cut[:,0].argsort(kind='mergesort')]
        new_circles.append(cut)
        
    # Replace circles with new_circles
    circles = np.concatenate(new_circles)

    cell_x, cell_y = 0, 0
    for center in circles:
        if(((center-radius*2)<0)|((center+radius*2)>dims)).any():
            continue
        x,y = center.ravel()
        well_sample =  image[y-radius:y+radius, x-radius:x+radius].copy()
        well_matrix[cell_y, cell_x, frame, :, :] = well_sample

        if cell_x > 0 and cell_x % (tray_width-1)  == 0:
            cell_x = 0
            cell_y += 1
        else:
            cell_x += 1
    

def find_wells(image):
    """
      Given a grayscale image of the assay, locate the wells with hough circles.
    """
    gray = image.copy()
    
    circles = cv2.HoughCircles(gray, 
                               cv2.HOUGH_GRADIENT_ALT, 
                               10.0, minDist = 130,
                               param1=110, param2=0.5,
                               minRadius=80, maxRadius=120)
    
    # For each detected circle
    if circles is not None:
      
        # Create a mask that stores the center of each detected circle.
        mask = np.zeros(gray.shape, dtype='uint8')
        for c in circles:
            c = c.ravel().astype('int')
            c_x, c_y, c_r = c
            
            # In this mask, we hardcode the radius to be 20 (but the exact constant doesn't matter).
            c_r = 20
            cv2.circle(mask, (c_x,c_y), c_r, (255), -1)

        # Now, we find the contours for each circle in the mask.
        # These contours are the zero'th output value, and are represented as an array of points.
        contours = cv2.findContours(mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        return contours[0]
        
    else:
        return None


def compute_np_well_matrix(folder_path, tray_width, tray_height, well_radius, A1_top):
    """
        Reads the well image dataset, and stores all well images in a 5D numpy array.
    """

    # Iterates through every image in the folder
    img_file_list = glob(folder_path+'/*.jpg')
    img_file_list.sort()

    num_frames = len(img_file_list)

    # We rotate and flip the tray in precomputation
    tray_width, tray_height = tray_height, tray_width
    well_matrix = np.zeros((tray_height, tray_width, num_frames, well_radius * 2, well_radius * 2))
    for frame, img_filename in enumerate(tqdm(img_file_list)):
        if frame >= num_frames:
            break

        img = cv2.imread(img_filename)
        plate = crop_to_plate(img)

        # If in the images, A1 is at the top, we need to rotate it so that it's at the bottom
        if A1_top == True:
            plate = cv2.rotate(plate, cv2.ROTATE_180)

        plate_rot = rotate_n_flip(plate)
        plate_cor = cor_lum(plate_rot)

        gray_plate_cor = cv2.cvtColor(plate_cor, cv2.COLOR_BGR2GRAY)
        circles = find_wells(gray_plate_cor)

        read_wells(gray_plate_cor, circles, well_matrix, frame, tray_width, well_radius, ret_img=True)


    return well_matrix
